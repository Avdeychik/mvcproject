//
//  GreenViewController.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 4.11.21.
//

import UIKit

class GreenViewController: UIViewController {
    
    private let tableView: UITableView = {
        let table = UITableView()
        return table
    }()
    
    var model = [AppModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        tableView.register(HeaderLabelCell.self, forCellReuseIdentifier: HeaderLabelCell.identifier)
        tableView.register(ContainerCell.self, forCellReuseIdentifier: ContainerCell.identifier)
        tableView.register(BottomLabelCell.self, forCellReuseIdentifier: BottomLabelCell.identifier)
        tableView.register(ButtonCell.self, forCellReuseIdentifier: ButtonCell.identifier)
        tableView.backgroundColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
        tableView.dataSource = self
        
        tableView.separatorStyle = .none
        tableView.bounces = false
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
}



extension GreenViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HeaderLabelCell.identifier, for: indexPath) as? HeaderLabelCell else { return UITableViewCell() }
        cell.headerLabel.text = "Allow traking on the next screen for:"
        return cell
        }
        else if indexPath.row == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ContainerCell.identifier, for: indexPath) as? ContainerCell else { return UITableViewCell() }
            cell.containerLabel.text = "Special offers and promotions just for you"
            cell.containerImage.image = UIImage(systemName: "heart.fill")
            return cell
        }
        else if indexPath.row == 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ContainerCell.identifier, for: indexPath) as? ContainerCell else { return UITableViewCell() }
            cell.containerLabel.text = "Advertisements that match your interests"
            cell.containerImage.image = UIImage(systemName: "hand.point.up.braille.fill")
            return cell
        }
        else if indexPath.row == 3 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ContainerCell.identifier, for: indexPath) as? ContainerCell else { return UITableViewCell() }
            cell.containerLabel.text = "An improved personalized experience over time"
            cell.containerImage.image = UIImage(systemName: "chart.bar.fill")
            return cell
        }
        else if indexPath.row == 4 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: BottomLabelCell.identifier, for: indexPath) as? BottomLabelCell else { return UITableViewCell() }
            cell.bottomLabel.text = "You can change this option later in the Settings app."
            return cell
        }
        else if indexPath.row == 5 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ButtonCell.identifier, for: indexPath) as? ButtonCell else { return UITableViewCell() }
            cell.continueButton = UIButton()
            return cell
        }
        
        
        return .init()
    }
}


//let model = arrayModel[indexPath.row]
//
//cell.fill(model: model, indexPath: indexPath)
