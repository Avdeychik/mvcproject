//
//  MyController.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 23.10.21.
//

import UIKit

protocol MyControllerOutput: AnyObject {
    func present(controller: UIViewController)
}

class MyController: UIViewController {
    
    let model: AppModel
    
    init(model: AppModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    override func loadView() {
        self.view = GreenView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let greenView = view as? GreenView else { return }
        model.setupScreen(view: greenView)
    }
}

extension MyController: MyControllerOutput {
    func present(controller: UIViewController) {
        present(controller, animated: true, completion: nil)
    }
}
