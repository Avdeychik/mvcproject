//
//  Constants.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 8.11.21.
//

import UIKit

let customGreenViewColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
let customPurpleViewColor = #colorLiteral(red: 0.4804907441, green: 0.3284894228, blue: 0.7497714162, alpha: 1)
