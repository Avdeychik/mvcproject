//
//  UIStackView.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 28.10.21.
//

import UIKit

extension UIStackView {
    convenience init(arrangedSubviews: [UIView], axsis: NSLayoutConstraint.Axis, spacing: CGFloat, distribution: UIStackView.Distribution) {
        self.init(arrangedSubviews: arrangedSubviews)
        self.axis = axsis
        self.spacing = spacing
        self.distribution = distribution
        self.translatesAutoresizingMaskIntoConstraints = false
    }
}
