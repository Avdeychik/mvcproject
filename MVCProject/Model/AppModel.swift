//
//  InfoModel.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 23.10.21.
//

import UIKit

class FeatureModel {
    
    var text: String
    var imageName: String
    
    init(text: String, imageName: String) {
        self.text = text
        self.imageName = imageName
    }
    
}

class AppModel {
    
    var someArray: [FeatureModel] = [
        FeatureModel(text: "Special offers and promotions just for you", imageName: "heart.fill"),
        FeatureModel(text: "Advertisements that match your interests", imageName: "hand.point.up.braille.fill"),
        FeatureModel(text: "An improved personalized experience over time", imageName: "chart.bar.fill"),
    ]
    
    weak var output: MyControllerOutput?
    
    let headerText: String
    let heartText: String
    let handPointText: String
    let chartBarText: String
    let bottomText: String
    
    let heartImageName: String
    let handPointImageName: String
    let chartBarImageName: String
    
    init(headerText: String, heartText: String, handPointText: String, chartBarText: String, bottomText: String, heartImageName: String, handPointImageName: String, chartBarImageName: String) {
        
        self.headerText = headerText
        self.heartText = heartText
        self.handPointText = handPointText
        self.chartBarText = chartBarText
        self.bottomText = bottomText
        self.heartImageName = heartImageName
        self.handPointImageName = handPointImageName
        self.chartBarImageName = chartBarImageName
    }
    
    func setupScreen(view: GreenView) {
        view.delegate = self
        view.fill(model: self)
    }
}

extension AppModel: GreenViewDelegate {
    
    func didTapContinueButton() {
        let model = AppModel(headerText: "Turning on location services allows us to provide features like:",
                             heartText: "Allerts when your pals are nearby",
                             handPointText: "News of events happening near you",
                             chartBarText: "Tagging and sharing your location",
                             bottomText: "You can change this later in the Settings app.",
                             heartImageName: "person.circle.fill",
                             handPointImageName: "flag.fill",
                             chartBarImageName: "location.circle.fill")
        
        
        let controller = MyController(model: model)
//        controller.view = PurpleView()
//        controller.modalPresentationStyle = .fullScreen
        model.output = controller
        
        output?.present(controller: controller)
    }
}


