//
//  ContainerModel.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 4.11.21.
//

import Foundation

class ContainerModel {
    
    let imageName: String
    let labelText: String
    
    init(imageName: String, labelText: String) {
        self.imageName = imageName
        self.labelText = labelText
    }
}
