//
//  BottomLabelCell.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 4.11.21.
//

import UIKit

class BottomLabelCell: UITableViewCell {
    
    static let identifier = "BottomLabelCell"
    
    let bottomLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 23, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        configureBottomLabel()
    }
    
    func configureBottomLabel() {
        addSubview(bottomLabel)
        bottomLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            bottomLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            bottomLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 30),
            bottomLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -90),
            bottomLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
            bottomLabel.heightAnchor.constraint(equalToConstant: 100)
        ])
    }
}
