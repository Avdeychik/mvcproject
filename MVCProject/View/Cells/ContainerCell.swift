//
//  GreenScreenTableViewCell.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 4.11.21.
//

import UIKit

class ContainerCell: UITableViewCell {
    
    static let identifier = "ContainerCell"
    
    let containerImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage()
        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let containerLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 23, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        configureContainerImage()
        configureContainerLabel()
    }
    
    func configureContainerImage() {
        addSubview(containerImage)
        containerImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            containerImage.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            containerImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 30),
            containerImage.heightAnchor.constraint(equalToConstant: 50),
            containerImage.widthAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    func configureContainerLabel() {
        addSubview(containerLabel)
        containerLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            containerLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            containerLabel.leftAnchor.constraint(equalTo: containerImage.rightAnchor, constant: 20),
            containerLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -50),
            containerLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
            containerLabel.heightAnchor.constraint(equalToConstant: 80)
        ])
    }
}
