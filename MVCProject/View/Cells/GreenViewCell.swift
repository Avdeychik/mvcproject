//
//  GreenViewCell.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 23.10.21.
//

import UIKit

class GreenViewCell: UITableViewCell {
    
    static let identifier = "GreenViewCell"
    
    //MARK: - UI Elements
    
    let headerLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 33, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    let bottomLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 23, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    private let continueButton: UIButton = {
        let button = UIButton(type: .system )
        button.setTitle("Continue", for: .normal)
        button.setTitleColor(.systemGreen, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 22, weight: .medium)
        button.backgroundColor = .white
        button.layer.cornerRadius = 35
        return button
    }()
    private var containerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .fill
        stack.spacing = 50
        return stack
    }()
    
    //MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureUI()
        backgroundColor = .gray
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    //MARK: - Fill
    
    func fill(model: AppModel, indexPath: IndexPath) {
    
        switch indexPath.row {
        case 0:
            headerLabel.text = model.headerText
        case 1:
            bottomLabel.text = model.bottomText
//        case 2:
        
        default:
            break
        }

//        someArray.forEach { feature in
//            let arrayLabel = HorizontalFeatureView(text: feature.text, image: UIImage(systemName:  feature.imageName))
//            containerStackView.addArrangedSubview(arrayLabel)
//        }
    }
    
    //MARK: - Configure UI
    
    private func configureUI() {
        configureHeaderLabel()
        configureContainerStackView()
//        configureBottomLabel()
//        configureContinueButton()
    }

    //header label
    
    func configureHeaderLabel() {
        addSubview(headerLabel)
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            headerLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 30),
            headerLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -50),
            headerLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
            headerLabel.heightAnchor.constraint(equalToConstant: 150)
            ])
    }
    
    func configureContainerStackView() {
        addSubview(bottomLabel)
        bottomLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            bottomLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            bottomLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 30),
            bottomLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -90),
            bottomLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
            bottomLabel.heightAnchor.constraint(equalToConstant: 100)
            ])
    }
    
//    // bottom label
//
//    func configureBottomLabel() {
//        addSubview(bottomLabel)
//        bottomLabel.translatesAutoresizingMaskIntoConstraints = false
//        NSLayoutConstraint.activate([
//            bottomLabel.topAnchor.constraint(equalTo: containerStackView.bottomAnchor, constant: 60),
//            bottomLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40),
//            bottomLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -70),
//        ])
//    }
//
//    //continue button
//
//    func configureContinueButton() {
//        addSubview(continueButton)
//        continueButton.translatesAutoresizingMaskIntoConstraints = false
//        NSLayoutConstraint.activate([
//            continueButton.topAnchor.constraint(equalTo: bottomLabel.bottomAnchor, constant: 50),
//            continueButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 20),
//            continueButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -20),
//            continueButton.heightAnchor.constraint(equalToConstant: 70),
//            continueButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -80)
//        ])
//    }
}

