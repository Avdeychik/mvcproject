//
//  HeaderLabelCell.swift
//  MVCProject
//  Created by Алексей Авдейчик on 4.11.21.
//

import UIKit

class HeaderLabelCell: UITableViewCell {
    
    static let identifier = "HeaderLabelCell"
    
    let headerLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 33, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        configureHeaderLabel()
    }
    
    func configureHeaderLabel() {
        addSubview(headerLabel)
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            headerLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 30),
            headerLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -50),
            headerLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
            headerLabel.heightAnchor.constraint(equalToConstant: 150)
        ])
    }
}


