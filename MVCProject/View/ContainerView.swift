//
//  ContainerView.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 1.11.21.
//

import UIKit

final class ContainerStackView: UIView, Feature{
    
    
    var textLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    var imageView: UIImageView = {
        let image = UIImageView()
        return image
    }()
    
    init(text: String, image: UIImage?) {
        self.textLabel.text = text
        self.imageView.image = image
        super.init(frame: .zero)
    }
    
    
    //    static let shared = ContainerkView()
    
    var containerImage: UIImageView = {
        let view = UIImageView()
        view.image = UIImage()
        view.tintColor = .white
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let containerLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 23, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        label.lineBreakMode = .byClipping
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.4
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var containerStackView = UIStackView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func createCustomContainerStackView(containerImageName: String, containerLabelText: String) -> UIStackView {
        
        containerImage.image = UIImage(systemName: (containerImageName))
        containerLabel.text = containerLabelText
        
        containerStackView = UIStackView(arrangedSubviews: [containerImage, containerLabel],
                                         axsis: .horizontal,
                                         spacing: 20,
                                         distribution: .fillProportionally)
        
        
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            
            containerImage.heightAnchor.constraint(equalToConstant: 50),
            containerImage.widthAnchor.constraint(equalToConstant: 50),
            containerImage.centerYAnchor.constraint(equalTo: containerStackView.centerYAnchor)
        ])
        
        return containerStackView
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
