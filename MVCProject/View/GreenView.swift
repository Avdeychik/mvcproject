//
//  MyView.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 23.10.21.
//

import UIKit

protocol GreenViewDelegate: AnyObject {
    func didTapContinueButton()
}

class GreenView: UIView {
    
    weak var delegate: GreenViewDelegate?
    
    //MARK: - UI Elements
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        return scrollView
    }()
    
    private let headerLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 33, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    private let bottomLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 23, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    private let continueButton: UIButton = {
        let button = UIButton(type: .system )
        let buttonColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
        button.setTitle("Continue", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 22, weight: .medium)
        button.setTitleColor(buttonColor, for: .normal)
        button.backgroundColor = .white
        button.layer.cornerRadius = 35
        button.addTarget(self, action: #selector(didTapContinueButton), for: .touchUpInside)
        return button
    }()
    
    private let containerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .fill
        stack.spacing = 50
        return stack
    }()
    
    //MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.3287599087, green: 0.5940672755, blue: 0.3663657308, alpha: 1)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    //MARK: - Action
    
    @objc func didTapContinueButton() {
        delegate?.didTapContinueButton()
    }
    
    //MARK: - Fill
    
    func fill(model: AppModel) {
        headerLabel.text = model.headerText
        bottomLabel.text = model.bottomText
        


        model.someArray.forEach { feature in
            let arrayLabel = HorizontalFeatureView(text: feature.text, image: UIImage(systemName:  feature.imageName))
            containerStackView.addArrangedSubview(arrayLabel)
        }
    }
    
    //MARK: - Configure UI
    
    private func configureUI() {
        configureScrollView()
        configureHeaderLabel()
        configureContainerStackView()
        configureBottomLabel()
        configureContinueButton()
    }
    
    //scroll view
    
    private func configureScrollView() {
        addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leftAnchor.constraint(equalTo: leftAnchor),
            scrollView.rightAnchor.constraint(equalTo: rightAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    //header label
    
    private func configureHeaderLabel() {
        scrollView.addSubview(headerLabel)
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 130),
            headerLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40),
            headerLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -50),
        ])
    }
    
    private func configureContainerStackView() {
        scrollView.addSubview(containerStackView)
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            containerStackView.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: 40),
            containerStackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 40),
            containerStackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -50),
        ])
    }
    
    // bottom label
    
    private func configureBottomLabel() {
        scrollView.addSubview(bottomLabel)
        bottomLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            bottomLabel.topAnchor.constraint(equalTo: containerStackView.bottomAnchor, constant: 60),
            bottomLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40),
            bottomLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -70),
        ])
    }
    
    //continue button
    
    private func configureContinueButton() {
        scrollView.addSubview(continueButton)
        continueButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            continueButton.topAnchor.constraint(equalTo: bottomLabel.bottomAnchor, constant: 50),
            continueButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 20),
            continueButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -20),
            continueButton.heightAnchor.constraint(equalToConstant: 70),
            continueButton.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -80)
        ])
    }
}

