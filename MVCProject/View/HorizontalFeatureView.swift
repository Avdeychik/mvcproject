//
//  HorizontalFeatureView.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 5.11.21.
//

import UIKit

protocol Feature: UIView {
   var textLabel: UILabel { get }
   var imageView: UIImageView { get }
   init(text: String, image: UIImage?)
}

final class HorizontalFeatureView: UIView, Feature {
    
    let imageView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage()
        image.tintColor = .white
        image.contentMode = .scaleAspectFit
        return image
    }()
    let textLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 23, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        return label
    }()
    
    init(text: String, image: UIImage?) {
        self.textLabel.text = text
        self.imageView.image = image
        super.init(frame: .zero)
        configureViews()
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    private func configureViews() {
        configureImageView()
        configureTextLabel()
    }
    
    private func configureImageView() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.leftAnchor.constraint(equalTo: leftAnchor),
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 50),
            imageView.widthAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func configureTextLabel() {
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(textLabel)
        NSLayoutConstraint.activate([
            textLabel.topAnchor.constraint(equalTo: topAnchor),
            textLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            textLabel.leftAnchor.constraint(equalTo: imageView.rightAnchor, constant: 16),
            textLabel.rightAnchor.constraint(equalTo: rightAnchor)
        ])
    }
}
