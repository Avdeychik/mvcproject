//
//  PurpleView.swift
//  MVCProject
//
//  Created by Алексей Авдейчик on 1.11.21.
//

import UIKit

class PurpleView: UIView {

    let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 35, weight: .bold)
        label.text = "Pal About"
        label.numberOfLines = 0
        label.backgroundColor = .white
        label.textColor = customPurpleViewColor
        label.lineBreakMode = .byClipping
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        label.sizeToFit()
        return label
    }()

    let titleView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()

    let headerLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 24, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        label.lineBreakMode = .byClipping
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.4
        return label
    }()
    let bottomLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 23, weight: .medium)
        label.numberOfLines = 0
        label.textColor = .white
        label.lineBreakMode = .byClipping
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.4
        return label
    }()
    let continueButton: UIButton = {
        let button = UIButton(type: .system )
        button.setTitle("Next", for: .normal)
        button.setTitleColor(customPurpleViewColor, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 22, weight: .medium)
        button.backgroundColor = .white
        button.layer.cornerRadius = 10
        return button
    }()
    let containerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .fill
        stack.spacing = 15
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    init() {
        super.init(frame: .zero)
        backgroundColor = customPurpleViewColor
        
//        containerStackView.arrangedSubviews
//        addSubview(containerStackView)
//        containerStackView.addArrangedSubview(topView)
//        containerStackView.addArrangedSubview(middleView)
//        containerStackView.addArrangedSubview(bottomView)

        configureUI()

        fill(model: AppModel(headerText: "Turning on location services allows us to provide features like:",
                             heartText: "Allerts when your pals are nearby",
                             handPointText: "News of events happening near you",
                             chartBarText: "Tagging and sharing your location",
                             bottomText: "You can change this later in the Settings app.",
                             heartImageName: "person.circle.fill",
                             handPointImageName: "flag.fill",
                             chartBarImageName: "location.circle.fill"))
    }
    required init?(coder: NSCoder) {
        return nil
    }


    func fill(model: AppModel) {
        headerLabel.text = model.headerText
        bottomLabel.text = model.bottomText
 }

    func configureUI() {
//        configureTitleLabel()
//        configureTitleView()
//        configureHeaderLabel()
//        configureContainerStackView()
//        configureContinueButton()
//        configureBottomLabel()
//        headerLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
    }

    func configureTitleView() {
        addSubview(titleView)
        titleView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: topAnchor),
            titleView.leftAnchor.constraint(equalTo: leftAnchor),
            titleView.rightAnchor.constraint(equalTo: rightAnchor),
            titleView.heightAnchor.constraint(equalToConstant: 130),
//            titleView.bottomAnchor.constraint(equalTo: headerLabel.topAnchor, constant: 20)
        ])
    }

    func configureTitleLabel() {
        titleView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(lessThanOrEqualTo: titleView.topAnchor, constant: 100),
            titleLabel.leftAnchor.constraint(equalTo: titleView.leftAnchor, constant: 30),
            titleLabel.widthAnchor.constraint(equalToConstant: 100),
            titleLabel.bottomAnchor.constraint(equalTo: titleView.bottomAnchor, constant: -10)
        ])
    }

    func configureHeaderLabel() {
        addSubview(headerLabel)
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            headerLabel.topAnchor.constraint(equalToSystemSpacingBelow: titleView.bottomAnchor, multiplier: 2),
            headerLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40),
            headerLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -60),
            headerLabel.bottomAnchor.constraint(equalTo: containerStackView.topAnchor, constant: -20)
        ])
    }

    func configureContainerStackView() {
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            containerStackView.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: 10),
            containerStackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 40),
            containerStackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -50),
//            containerStackView.bottomAnchor.constraint(equalTo: bottomLabel.topAnchor, constant: -10),
            containerStackView.heightAnchor.constraint(lessThanOrEqualToConstant: 250)
        ])
    }

    func configureContinueButton() {
        addSubview(continueButton)
        continueButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
//            continueButton.topAnchor.constraint(equalTo: containerStackView.bottomAnchor, constant: 20),
            continueButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 20),
            continueButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -20),
//            continueButton.bottomAnchor.constraint(equalTo: bottomLabel.topAnchor, constant: -10),
            continueButton.heightAnchor.constraint(greaterThanOrEqualToConstant: 70),
        ])
    }

    func configureBottomLabel() {
        addSubview(bottomLabel)
        bottomLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            bottomLabel.topAnchor.constraint(lessThanOrEqualTo: continueButton.bottomAnchor, constant: 20),
            bottomLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40),
            bottomLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -40),
            bottomLabel.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -20)
        ])
    }
}
